﻿using System;
using RotaGeek.PublicModel.Enums;
using RotaGeek.PublicModel.Interface;
using RotaGeek.RESTWeb.Api.Client.Models;
using RotaGeek.TestData;

namespace RotaGeek.PublicModel.TestData
{
    public static class SwapRequestTestData
    {
        private static MiniSwapRequest<IMiniJournal> Builder(DateTime startDateTime, DateTime endDateTime)
        {
            return new MiniSwapRequest<IMiniJournal>()
            {

            };

        }

		public static IMiniSwapRequest<IMiniJournal, SwapRequestStatus> ApprovedSwapRequestForTomorrow()
        {
            return new MiniSwapRequest<IMiniJournal>()
            {
                StatusId = (int)SwapRequestStatus.Accepted,
                
				JournalEntry = (MiniJournal)JournalTestData.DayShiftTomorrow(DateTime.Today),

                Notes = "Some noteses for this swap"

            };
        }

		public static IMiniSwapRequest<IMiniJournal, SwapRequestStatus> ApprovedSwapRequestForNextWeek()
        {
            return new MiniSwapRequest<IMiniJournal>()
            {
                StatusId = (int)SwapRequestStatus.Accepted,

				JournalEntry = (MiniJournal)JournalTestData.DayShiftTwoDays(DateTime.Today),

                Response = "Response to this swap request"
            };
        }

		public static IMiniSwapRequest<IMiniJournal,SwapRequestStatus> ApprovedSwapRequestThreeDays()
        {
            return new MiniSwapRequest<IMiniJournal>()
            {
                StatusId = (int)SwapRequestStatus.Accepted,

				JournalEntry = JournalTestData.DayShiftThreeDays(DateTime.Today)
            };
        }


		public static IMiniSwapRequest<IMiniJournal, SwapRequestStatus> InProcessOrOutstandingSwapRequestForTwoDays()
        {
            return new MiniSwapRequest<IMiniJournal>()
            {
                StatusId = (int)SwapRequestStatus.InProcess,

				JournalEntry = (MiniJournal)JournalTestData.DayShiftTwoDays(DateTime.Today)
            };
        }

		public static IMiniSwapRequest<IMiniJournal, SwapRequestStatus> RejectedSwapRequestForNextWeek()
        {
            return new MiniSwapRequest<IMiniJournal>()
            {
                StatusId = (int)SwapRequestStatus.Rejected,

				JournalEntry = (MiniJournal)JournalTestData.DayShiftToday(DateTime.Today)
            };
        }

    }
}
