﻿using RotaGeek.PublicModel.Interface;
using RotaGeek.RESTWeb.Api.Client.Models;

namespace RotaGeek.PublicModel.TestData
{
    public static class UserTestData
    {
        public static IUserProfileSettings GetUserProfileSettings()
        {
            return new UserProfileSettings()
            {
                Email = "andrew@rotageek.com",
                Mobile = "0123456789",
                ReceiveByEmail = true,
                ReceiveBySMS = false
            };
        }
    }
}
