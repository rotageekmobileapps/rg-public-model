﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RotaGeek.PublicModel.Enums;
using RotaGeek.PublicModel.Interface;
using RotaGeek.RESTWeb.Api.Client.Models;

namespace RotaGeek.TestData
{
    public static class LeaveRequestTestData
    {
        private static MiniLeaveRequest Builder(DateTime startDateTime, DateTime endDateTime)
        {
            return new MiniLeaveRequest()
            {

            };
        }


        public static IMiniLeaveRequest ApprovedLeaveRequestTomorrow()
        {
            return new MiniLeaveRequest()
            {
                StatusId = (int)LeaveStatus.Approved,

				LeaveStartOn = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 0,0,0,DateTimeKind.Local),
				LeaveEndOn   = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23,59,0,DateTimeKind.Local),

				Description = "Reason for the swap",

                //LeaveTypeId = 46190 //Annual Leave - Account Specific
            };
        }

        public static IMiniLeaveRequest InProcessLeaveEventInTwoDays()
        {
            return new MiniLeaveRequest()
            {
                StatusId = (int)LeaveStatus.InProcess,

                LeaveStartOn = DateTime.Today.AddDays(2).AddHours(9),
                LeaveEndOn = DateTime.Today.AddDays(2).AddHours(17),

                //LeaveTypeId = 46194, //Leui Day - Account Specific

                Notes = "Some notes on the request"
            };
        }

        public static IMiniLeaveRequest NotApprovedLeaveRequestInThreeDays()
        {
            return new MiniLeaveRequest()
            {
                StatusId = (int)LeaveStatus.NotApproved,

                LeaveStartOn = DateTime.Today.AddDays(3).AddHours(9),
                LeaveEndOn = DateTime.Today.AddDays(3).AddHours(17),

                //LeaveTypeId = 46193, //Sick Day - Account Specific

                Response = "Repsonse to the sick day leave request"
            };
        }

        public static IMiniLeaveRequest OutstandingLeaveEventInFourDays()
        {
            return new MiniLeaveRequest()
            {
                StatusId = (int)LeaveStatus.Outstanding,

                LeaveStartOn = DateTime.Today.AddDays(4).AddHours(9),
                LeaveEndOn = DateTime.Today.AddDays(4).AddHours(17),

                //LeaveTypeId = 46191 //Study Leave - Account Specific
            };
        }
    }
}
