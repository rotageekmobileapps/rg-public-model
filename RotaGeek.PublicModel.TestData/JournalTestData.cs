﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using RotaGeek.PublicModel;
using RotaGeek.PublicModel.Interface;
using RotaGeek.RESTWeb.Api.Client.Models;
using MiniLeaveRequest = RotaGeek.PublicModel.Poco.MiniLeaveRequest;

namespace RotaGeek.TestData
{
    public static class JournalTestData
    {

        private static MiniJournal Builder(DateTime startDate, DateTime endDate)
        {
            return new MiniJournal()
            {
                
            };
        }

		public static IMiniJournal DayShiftToday(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Day",
                Location = "London",

				StartDateTime = date.AddHours(9),
				EndDateTime = date.AddHours(17),

                HasCheckedIn = true,
                HasCheckedOut = true,

                name = "Thomas Cartwright",

                Colour="5139d9",

                CanRequestDayOff = false,
                CanRequestSwap = false,

                Icon="time",

                HasSwap = false,
				JournalId = CreateUniqueId(1001, date)
            };
        }

 

		public static MiniJournal DayShiftTomorrow(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Day",
                Location = "London",

				StartDateTime = date.AddDays(1).AddHours(9),
				EndDateTime = date.AddDays(1).AddHours(17),

                HasSwap = true,
                
                Colour = "5139d9",

                CanRequestSwap = false,
                CanRequestDayOff = true,

                name = "Thomas Cartwright",

                Icon = "plane",

                HasCheckedIn = false,
                HasCheckedOut = false,
				JournalId = CreateUniqueId(1002, date)
            };
        }

		public static IMiniJournal DayShiftTwoDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Evening",
                Location = "Edinburgh",

				StartDateTime = date.AddDays(2).AddHours(18),
				EndDateTime = date.AddDays(3),

                CanRequestDayOff = true,
                CanRequestSwap = true,

                name = "Thomas Cartwright",

                HasSwap = false,

                Icon = "bolt",

                HasCheckedIn = false,
                HasCheckedOut = false,

				Colour = "5139d9",
				JournalId = CreateUniqueId(1003, date)
            };
        }

		public static IMiniJournal DayShiftSixDays(DateTime date)
		{
			return new MiniJournal()
			{
				Description = "Morning",
				Location = "Stockholm",

				StartDateTime = date.AddDays(6).AddHours(9),
				EndDateTime = date.AddDays(7),

				CanRequestDayOff = true,
				CanRequestSwap = true,

				name = "Thomas Cartwright",

				HasSwap = false,

				Icon = "bolt",

				HasCheckedIn = false,
				HasCheckedOut = false,

				Colour = "5139d9",
				JournalId = CreateUniqueId(1004, date)
			};
		}

		static int CreateUniqueId(int id, DateTime date)
		{
			string dayId = "00" + date.Day.ToString();
			string fullId = id.ToString() + dayId.Substring(dayId.Length - 2);
			return Int32.Parse(fullId);
		}

		public static IMiniJournal OvernightShiftToday(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Overnight",
                Location = "London",
                Colour = "428a9a",
                

				StartDateTime = date.AddHours(20),
				EndDateTime = date.AddHours(28),

                HasCheckedIn = false,
                HasCheckedOut = false,

                name = "Thomas Cartwright",

                Icon = "truck",

                HasSwap = false,

                CanRequestDayOff = true,
                CanRequestSwap = true,
				JournalId = CreateUniqueId(1005, date)
            };
        }

		public static IMiniJournal SickLeaveThreeDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Sick Leave",
                Location = "London",

				StartDateTime = date.AddDays(3).AddHours(9),
				EndDateTime = date.AddDays(3).AddHours(17),

                HasCheckedIn = false,
                HasCheckedOut = false,

                name = "Thomas Cartwright",

                HasSwap = false,

                Icon = "glass",
                Colour = "324b74",

                CanRequestDayOff = false,
				CanRequestSwap = false,
				JournalId = CreateUniqueId(1006, date)

                
            };
        }

		public static IMiniJournal AnnualLeaveFiveDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Annual Leave",
                Location = "London",

				StartDateTime = date.AddDays(5).AddHours(9),
				EndDateTime = date.AddDays(5).AddHours(17),

                HasSwap = false,

                HasCheckedIn = false,
                HasCheckedOut = false,

                name = "Thomas Cartwright",

                Icon = "trophy",
                Colour = "324b74",

                CanRequestDayOff = false,
				CanRequestSwap = false,
				JournalId = CreateUniqueId(1007, date)
            };
        }

		public static IMiniJournal StudyLeaveFourDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Study Leave",
                Location = "London",

                StartDateTime = date.AddDays(4).AddHours(9),
				EndDateTime = date.AddDays(4).AddHours(17),

                HasSwap = false,

                name = "Thomas Cartwright",

                HasCheckedIn = false,
                HasCheckedOut = false,

                Icon = "picture",
                Colour = "324b74",

                CanRequestDayOff = false,
				CanRequestSwap = false,
				JournalId = CreateUniqueId(1008, date)
            };
        }

		public static IMiniJournal AnnualLeaveSixDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Annual Leave",
                Location = "London",

				StartDateTime = date.AddDays(6).AddHours(9),
				EndDateTime = date.AddDays(6).AddHours(17),

                HasSwap = false,

                HasCheckedIn = false,
                HasCheckedOut = false,

                name = "Thomas Cartwright",

                Icon = "dollar",
                Colour = "c72ac8",

                CanRequestDayOff = false,
				CanRequestSwap = false,
				JournalId = CreateUniqueId(1009, date)
            };
        }


		public static IMiniJournal DayShiftFourDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "One Week Ago",
                Location = "London",

                StartDateTime = date.AddDays(4).AddHours(9),
                EndDateTime = date.AddDays(4).AddHours(17),

                Colour="24ae3b",

                Icon = "bell",

                name = "Thomas Cartwright",

                HasSwap = false,

                HasCheckedIn = false,
                HasCheckedOut = false,

                CanRequestDayOff = false,
				CanRequestSwap = false,
				JournalId = CreateUniqueId(1010, date)
            };
        }

		public static MiniJournal DayShiftThreeDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Next Week",
                Location = "London",
                
                StartDateTime = date.AddDays(3).AddHours(9),
                EndDateTime = date.AddDays(3).AddHours(17),

                Colour = "24ae3b",

                Icon = "archive",

                name = "Thomas Cartwright",

                HasSwap = false,

                HasCheckedIn = false,
                HasCheckedOut = false,

                CanRequestDayOff = true,
				CanRequestSwap = true,
				JournalId = CreateUniqueId(1011, date)
            };
        }

		public static IMiniJournal NightShiftFourDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Next Week",
                Location = "London",

                StartDateTime = date.AddDays(4).AddHours(19),
                EndDateTime = date.AddDays(4).AddHours(23),

                Colour = "24ae3b",

                Icon = "sun",

                HasSwap = false,

                name = "Thomas Cartwright",

                HasCheckedIn = false,
                HasCheckedOut = false,

                CanRequestDayOff = true,
				CanRequestSwap = true,
				JournalId = CreateUniqueId(1012, date)
            };
        }

		public static IMiniJournal NightShiftToday(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "One Week Ago",
                Location = "London",

                StartDateTime = date.AddHours(19),
                EndDateTime = date.AddHours(23),

                Colour = "24ae3b",

                Icon = "moon",

                HasSwap = false,

                name = "Thomas Cartwright",

                HasCheckedIn = false,
                HasCheckedOut = false,

                CanRequestDayOff = false,
				CanRequestSwap = false,
				JournalId = CreateUniqueId(1013, date)
            };
        }

		public static IMiniJournal NightShiftTwoDays(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Evening",
                Location = "Edinburgh",

                StartDateTime = date.AddDays(2).AddHours(18),
                EndDateTime = date.AddDays(2).AddHours(23),

                CanRequestDayOff = true,
                CanRequestSwap = true,

                HasSwap = false,

                name = "Thomas Cartwright",

                Icon = "medkit",

                HasCheckedIn = false,
                HasCheckedOut = false,

				Colour = "5139d9",
				JournalId = CreateUniqueId(1014, date)
            };
        }

		public static IMiniJournal SwappedShift(DateTime date)
		{
			return new MiniJournal()
			{
				Description = "swapped shift",
				Location = "Edinburgh",

				StartDateTime = date.AddDays(4).AddHours(18),
				EndDateTime = date.AddDays(4).AddHours(23),

				CanRequestDayOff = true,
				CanRequestSwap = true,

				HasSwap = true,

				name = "Thomas Cartwright",

				Icon = "medkit",

				HasCheckedIn = false,
				HasCheckedOut = false,

				Colour = "5139d9",
				JournalId = CreateUniqueId(1015, date)
			};
		}

		public static IMiniJournal SwappedShiftWithNotes(DateTime date)
		{
			return new MiniJournal()
			{
				Description = "Swapped shift with notes",
				Location = "Edinburgh",

				StartDateTime = date.AddDays(2).AddHours(18),
				EndDateTime = date.AddDays(2).AddHours(23),

				CanRequestDayOff = true,
				CanRequestSwap = true,

				HasSwap = true,
				Notes = new List<string> {"These are notes"},

				name = "Thomas Cartwright",

				Icon = "medkit",

				HasCheckedIn = false,
				HasCheckedOut = false,

				Colour = "5139d9",
				JournalId = CreateUniqueId(1016, date)
			};
		}

		public static IMiniJournal ShiftWithNotes(DateTime date)
		{
			return new MiniJournal()
			{
				Description = "Shift with notes",
				Location = "Edinburgh",

				StartDateTime = date.AddDays(2).AddHours(18),
				EndDateTime = date.AddDays(2).AddHours(23),

				CanRequestDayOff = true,
				CanRequestSwap = true,

				HasSwap = false,
				Notes = new List<string> {"These are notes","More notes", "really long line of notes, that will wrap onto more than one line, and perahps even two but that would be crazy"},

				name = "Thomas Cartwright",

				Icon = "medkit",

				HasCheckedIn = false,
				HasCheckedOut = false,

				Colour = "5139d9",
				JournalId = CreateUniqueId(1017, date)
			};
		}

		public static IMiniJournal MorningShiftTodayForAndrew(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Day",
                Location = "London",

                StartDateTime = date.AddHours(1),
                EndDateTime = date.AddHours(4),

                HasCheckedIn = true,
                HasCheckedOut = true,

                Colour = "5139d9",

                CanRequestDayOff = false,
                CanRequestSwap = false,

                name = "Andrew Jefferson",

                Icon = "time",

				HasSwap = false,
				JournalId = CreateUniqueId(1018, date)
            };
        }

		public static IMiniJournal MorningShiftTomorrowForAndrew(DateTime date)
        {
            return new MiniJournal()
            {
                Description = "Day",
                Location = "London",

				StartDateTime = date.AddDays(1).AddHours(9),
				EndDateTime = date.AddDays(1).AddHours(17),

                HasCheckedIn = true,
                HasCheckedOut = true,

                Colour = "5139d9",

                CanRequestDayOff = false,
                CanRequestSwap = false,

                name = "Andrew Jefferson",

                Icon = "time",

				HasSwap = false,
				JournalId = CreateUniqueId(1019, date)
            };
        }

		public static IMiniJournal MatchingStartEndDate(DateTime date)
		{
			return new MiniJournal()
			{
				Description = "Matching start/end",
				Location = "London",

				StartDateTime = date.AddDays(1).AddHours(9),
				EndDateTime = date.AddDays(1).AddHours(9),

				HasCheckedIn = true,
				HasCheckedOut = true,

				Colour = "5139d9",

				CanRequestDayOff = false,
				CanRequestSwap = false,

				name = "Neil Pepper",

				Icon = "time",

				HasSwap = false,
				JournalId = CreateUniqueId(1020, date)
			};
		}


        //TODO: Put in seperate class LeaveRequestTestData
        public static IMiniLeaveRequest LeaveRequestToday()
        {
            return new MiniLeaveRequest()
            {

            };
        }
    }
}
