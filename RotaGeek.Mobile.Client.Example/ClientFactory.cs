﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using RotaGeek.Mobile.Client.Mock;
using RotaGeek.PublicModel;
using RotaGeek.PublicModel.Interface;
using RotaGeek.REST.Client;

namespace RotaGeek.Mobile.Client.Example
{
    public class ClientFactory
    {
        private readonly string _mockUsername;
        public readonly bool UseMockClient;

        public ClientFactory(string mockUsername, bool useMock)
        {
            _mockUsername = mockUsername;
            UseMockClient = useMock;
        }

        public async Task<IRgClient> BuildClientAsync(AppDefinition appDefinition, LoginModel loginModel, Action<FlavouredRefreshToken> tokenCallback, HttpMessageHandler messageHandler = null)
        {
            return  UseMockClient ? await Task.Run(() =>  (IRgClient)new MockRgClient(loginModel.Username, loginModel.Password)) : await RgClient.Create(appDefinition, loginModel, tokenCallback, messageHandler);
        }

        public async Task<IRgClient> BuildClientAsync(AppDefinition appDefinition, FlavouredRefreshToken token, Action<FlavouredRefreshToken> tokenCallback, HttpMessageHandler messageHandler = null)
        {
            return UseMockClient ? await Task.Run(() => (IRgClient)new MockRgClient(_mockUsername, token.RefreshToken.Value)) : await RgClient.Create(appDefinition, token, tokenCallback, messageHandler);
        }

        /*
        private static IRgDataClient GetRealClient(string username, string password)
        {
            var client = new MobileApiClient();
            client.SetLoggedInUser(username, password);
            return client.Wrap();
        }
        */
    }
}