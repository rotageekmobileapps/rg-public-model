﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using RotaGeek.PublicModel.Poco;
using RotaGeek.RESTWeb.Api.Client;

namespace RotaGeek.Mobile.Client.Example
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Configuration.RgClientBaseAddress = "http://rgrestapi.azurewebsites.net/";
            var clientFactory = new ClientFactory(null,false);
            ClientDemo.DoStuff(clientFactory).Wait();
            clientFactory = new ClientFactory("test", true);
            ClientDemo.DoStuff(clientFactory).Wait();
            Console.ReadLine();
        }
    }
}
