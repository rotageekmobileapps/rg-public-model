﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RotaGeek.PublicModel;
using RotaGeek.PublicModel.Interface;
using RotaGeek.PublicModel.Poco;
using RotaGeek.REST.Client;

namespace RotaGeek.Mobile.Client.Example
{
    class ClientDemo
    {
        public static void PersistToken(FlavouredRefreshToken tokenToPersist)
        {
            Task.Run(() =>
            {
                //Persist it
            });
        }
        public static async Task DoStuff(ClientFactory clientFactory)
        {
            FlavouredRefreshToken refreshToken = new FlavouredRefreshToken(new RefreshToken(null, null), null);
            using (var client = await clientFactory.BuildClientAsync(new AppDefinition("iPhone 4", "iOS 7.1", "RotaGeek", "1.0"), new LoginModel() { Username = "a.jefferson11", Password = "test123"},
                t =>
                {
//                    refreshToken.UpdateFrom(t);
                    PersistToken(refreshToken);
                }))
            {
                await DoStuffInternal(client);
            }

            using (var client = await clientFactory.BuildClientAsync(new AppDefinition("iPhone 4", "iOS 7.1", "RotaGeek", "1.0"), refreshToken,
                t =>
                {
//                    refreshToken.UpdateFrom(t);
                    PersistToken(refreshToken);
                }))
            {
                await DoStuffInternal(client);
            }
        }

        private static async Task DoStuffInternal(IRgClient client)
        {
            var dataClient = client.DataClient;
            var promiseJournals =
                dataClient.JournalEntriesForUserAsync(DateTime.Today.AddYears(-1), DateTime.Today.AddDays(5));

            var resultOfPromiseJournals = await promiseJournals;

            var promiseJournalsForWeek =
                dataClient.JournalEntriesForWeekViewAsync(DateTime.Today.AddYears(-1), DateTime.Today.AddDays(5));

            var resultOfPromiseWeekViewJournals = await promiseJournalsForWeek;

            var promiseLeave =
                dataClient.LeaveRequestsForUserAsync();

            var resultOfPromiseLeave = await promiseLeave;

            var promiseSwap =
                dataClient.SwapRequestsForUserAsync();

            var resultOfPromiseSwap = await promiseSwap;

            
            var promiseSwapRequest =
                dataClient.IssueSwapRequestAsyncMsg(new IssueSwapRequestModel()
                {
                    Journal = resultOfPromiseJournals.First()
                });
            
            var resultOfPromiseSwapRequest = await promiseSwapRequest;

            var locations = await dataClient.LocationsForUserAsync();

            var leaveTypes = await dataClient.LeaveTypesForLocationAsync(locations.First().LocationId);
            
            var promiseLeaveRequest =
                dataClient.IssueLeaveRequestAsync(new IssueLeaveRequestModel()
                {
                    LeaveStartOn = DateTime.Today.AddDays(5),
                    LeaveEndOn = DateTime.Today.AddDays(7),
                    LeaveTypeId = leaveTypes.First().LeaveTypeId,
                    Reason = "Testing"
                });

            var resultOfPromiseLeaveRequest = await promiseLeaveRequest;
            

            var promiseTaStatus =
                dataClient.TAForUserAsync(DateTime.Today, DateTime.Today.AddDays(1));

            var resultOfPromiseTaStatus = await promiseTaStatus;

            var promiseUserSettings = dataClient.UserProfileSettingsAsync();

            var userSettings = await promiseUserSettings;

            userSettings.ReceiveByEmail = !userSettings.ReceiveByEmail;
            userSettings.Email = $"thomas+{Guid.NewGuid()}@rotageek.com";
            var promiseUpdateUserSettings = dataClient.UserProfileSettingsAsync(userSettings);

            var userSettingsUpdate = await promiseUpdateUserSettings;

            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(resultOfPromiseJournals));
            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(resultOfPromiseLeave));
            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(resultOfPromiseSwap));
            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(userSettings));
            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(userSettingsUpdate));

            if (!resultOfPromiseSwapRequest.IsSuccessStatusCode) throw new Exception("error");
            Console.WriteLine(resultOfPromiseLeaveRequest);
            
            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(resultOfPromiseWeekViewJournals));
            Console.WriteLine(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(resultOfPromiseTaStatus));
        }
    }
}
