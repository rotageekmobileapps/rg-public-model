﻿using System;

namespace RotaGeek.Mobile.Client.Exceptions
{
    public class AccessDeniedException : Exception
    {
        public readonly string UserMessage;
        public AccessDeniedException(string username, string userMessage) : base("Access denied for user: " + username)
        {
            UserMessage = userMessage;
        }
    }
}