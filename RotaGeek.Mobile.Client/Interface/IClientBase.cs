using System;
using System.Net.Http;

namespace RotaGeek.Mobile.Client.Interface
{
    public interface IClientBase : IDisposable
    {
        HttpClient HttpClient { get; }
    }
}