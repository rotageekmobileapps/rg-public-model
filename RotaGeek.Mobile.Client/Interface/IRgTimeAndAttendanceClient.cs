﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using RotaGeek.PublicModel;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.Mobile.Client.Interface
{
    public interface IRgTimeAndAttendanceClient : IDisposable
    {
        Task<IMiniJournal> GetCurrentShiftAsync();
    }
}