﻿using System;

namespace RotaGeek.Mobile.Client.Interface
{
    public interface IRgAuthClient : IDisposable
    {
        string Authenticate(string username, string password);

        //TODO: Token might become an object
        string Authenticate(string token);
    }
}