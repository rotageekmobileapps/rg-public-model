﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.Mobile.Client.Interface
{
    public interface IRgUserClient : IDisposable
    {
        Task<IRgUserSettings> SettingsForUserAsync();
        Task<IRgUserConfiguration> ConfigurationForUserAsync();
        Task<HttpResponseMessage> UpdateUserSettings(IRgUserSettings settings);
    }
}