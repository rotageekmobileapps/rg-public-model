﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using RotaGeek.Mobile.Client.Exceptions;
using RotaGeek.PublicModel;
using RotaGeek.PublicModel.Enums;
using RotaGeek.PublicModel.Interface;
using RotaGeek.PublicModel.TestData;
using RotaGeek.RESTWeb.Api.Client.Models;
using RotaGeek.TestData;
using IIssueCheckOutRequest = RotaGeek.PublicModel.Interface.IIssueCheckOutRequest;
using IJournalIdentifiable = RotaGeek.PublicModel.Interface.IJournalIdentifiable;
using TAStatus = RotaGeek.PublicModel.Poco.TAStatus;
using System.Linq;

namespace RotaGeek.Mobile.Client.Mock
{
    /// <summary>
    ///     A mock implementation of the client data interface
    /// </summary>
    public class MockRgClient :
        IRgDataClient, IRgClient
    {
        private readonly IUserProfileSettings _userProfileSettings = UserTestData.GetUserProfileSettings();
		List<ITAStatus> TAStatusList;

        public MockRgClient(string username, string password)
        {
            long l;
            if (!(username == password || long.TryParse(password, out l)))
            {
                throw new AccessDeniedException(username,
                    "username must equal password or password must be an valid long for mock auth");
            }
        }

        public IRgDataClient DataClient => this;

        public async Task<List<IMiniJournal>> JournalEntriesForUserAsync(DateTime datefrom, DateTime dateto)
        {
            //TODO: Only grab shifts between dates
            return await Task.Run(() =>
            {
                if (DateTime.Today <= dateto && DateTime.Today >= datefrom)
                {
                    return new List<IMiniJournal>
                    {
                        JournalTestData.DayShiftToday(datefrom),
                        JournalTestData.OvernightShiftToday(datefrom),
                        JournalTestData.SickLeaveThreeDays(datefrom),
                        JournalTestData.DayShiftTwoDays(datefrom),
                        JournalTestData.NightShiftTwoDays(datefrom),
                        JournalTestData.NightShiftFourDays(datefrom),
                        JournalTestData.SwappedShift(datefrom),
                        JournalTestData.SwappedShiftWithNotes(datefrom),
                        JournalTestData.ShiftWithNotes(datefrom),
                        JournalTestData.AnnualLeaveSixDays(datefrom),
						JournalTestData.MatchingStartEndDate(datefrom)

                    };
                }
                return new List<IMiniJournal>
                {
                    JournalTestData.NightShiftFourDays(datefrom),
                    JournalTestData.DayShiftTwoDays(datefrom),
                    JournalTestData.AnnualLeaveFiveDays(datefrom),
                    JournalTestData.AnnualLeaveSixDays(datefrom)
                };
            });
        }

        public async Task<List<IMiniJournal>> JournalEntriesForWeekViewAsync(DateTime datefrom, DateTime dateto)
        {
            return await Task.Run(() =>
            {
                if (DateTime.Today <= dateto && DateTime.Today >= datefrom)
                {
                    return new List<IMiniJournal>
                    {
                        JournalTestData.DayShiftToday(datefrom),
                        JournalTestData.OvernightShiftToday(datefrom),
                        JournalTestData.SickLeaveThreeDays(datefrom),
                        JournalTestData.DayShiftTwoDays(datefrom),
						JournalTestData.DayShiftSixDays(datefrom),
                        JournalTestData.NightShiftTwoDays(datefrom),
                        JournalTestData.NightShiftFourDays(datefrom),
                        JournalTestData.NightShiftToday(datefrom),
                        JournalTestData.AnnualLeaveFiveDays(datefrom),
                        JournalTestData.MorningShiftTodayForAndrew(datefrom),
                        JournalTestData.MorningShiftTomorrowForAndrew(datefrom),
                        JournalTestData.StudyLeaveFourDays(datefrom),
						JournalTestData.MatchingStartEndDate(datefrom)
//						JournalTestData.AnnualLeaveNextWeek(datefrom),
//						JournalTestData.AnnualLeaveSixDaysAgo(datefrom)
                    };
                }
                return new List<IMiniJournal>
                {
                    JournalTestData.StudyLeaveFourDays(datefrom),
                    JournalTestData.DayShiftTomorrow(datefrom),
                    JournalTestData.AnnualLeaveFiveDays(datefrom),
                    JournalTestData.AnnualLeaveSixDays(datefrom),
                    JournalTestData.MorningShiftTomorrowForAndrew(datefrom)
                };
            });
        }

        public async Task<List<IMiniSwapRequest<SwapRequestStatus>>> SwapRequestsForUserAsync()
        {
            //TODO: Only grab shifts from the future
            return await Task.Run(() =>
            {
                return new List<IMiniSwapRequest<SwapRequestStatus>>

                {
                    SwapRequestTestData.ApprovedSwapRequestForTomorrow(),
                    SwapRequestTestData.ApprovedSwapRequestForNextWeek(),
                    SwapRequestTestData.InProcessOrOutstandingSwapRequestForTwoDays(),
                    SwapRequestTestData.RejectedSwapRequestForNextWeek()
                };
            });
        }

        public async Task<List<IMiniLeaveRequest>> LeaveRequestsForUserAsync()
        {
            return await Task.Run(() =>
            {
                return new List<IMiniLeaveRequest>
                {
                    LeaveRequestTestData.ApprovedLeaveRequestTomorrow(),
                    LeaveRequestTestData.InProcessLeaveEventInTwoDays(),
                    LeaveRequestTestData.NotApprovedLeaveRequestInThreeDays(),
                    LeaveRequestTestData.OutstandingLeaveEventInFourDays()
                };
            });
        }
			
        public async Task<HttpResponseMessage> IssueSwapRequestAsyncMsg(
            IIssueSwapRequestModel<IJournalIdentifiable> issueSwapRequestModel)
        {
            return await Task.Run(() => { return new HttpResponseMessage(HttpStatusCode.OK); });
        }

        public async Task<string> IssueLeaveRequestAsync(IIssueLeaveRequestModel issueLeaveRequestModel)
        {
            return await Task.FromResult("Your leave request has been submitted to your supervisor for approval.");
        }

        public Task<IUserProfileSettings> UserProfileSettingsAsync()
        {
            return Task.FromResult(_userProfileSettings);
        }

        public Task<IUserProfileSettings> UserProfileSettingsAsync(IUserProfileSettings userProfileSettings)
        {
            _userProfileSettings.Mobile = userProfileSettings.Mobile ?? _userProfileSettings.Mobile;
            _userProfileSettings.Email = userProfileSettings.Email ?? _userProfileSettings.Email;
            _userProfileSettings.ReceiveByEmail = userProfileSettings.ReceiveByEmail ??
                                                  _userProfileSettings.ReceiveByEmail;
            _userProfileSettings.ReceiveBySMS = userProfileSettings.ReceiveBySMS ?? _userProfileSettings.ReceiveBySMS;
            return Task.FromResult(_userProfileSettings);
        }

        public Task<List<IMiniLeaveType>> LeaveTypesForLocationAsync(string locationId)
        {
            List<IMiniLeaveType> miniLeaveTypes;
            switch (locationId)
            {
                case "1":
                    miniLeaveTypes = new List<IMiniLeaveType>
                    {
                        new MiniLeaveType
                        {
                            LeaveTypeId = 1,
                            Color = "5139d9",
                            Description = "Annual Leave",
                            Icon = "plane"
                        }
                    };
                    break;
                default:
                    miniLeaveTypes = new List<IMiniLeaveType>
                    {
                        new MiniLeaveType
                        {
                            LeaveTypeId = 2,
                            Color = "324b74",
                            Description = "Sick Leave",
                            Icon = "medkit"
                        }
                    };
                    break;
            }


            return Task.FromResult(miniLeaveTypes);
        }

        public Task<List<IMiniLocation>> LocationsForUserAsync()
        {
            var locations = new List<IMiniLocation>()
            {
                new MiniLocation()
                {
                    LocationId 	  = "1",
                    Name 		  = "Head Office",
					Latitude 	  = 52.13960134,
					Longitude 	  = -0.45851555,
					CheckInRadius = 25
                },
                new MiniLocation()
                {
                    LocationId 	  = "2",
                    Name 		  = "Starbucks",
					Latitude 	  = 50.986748,  
					Longitude 	  = 0.4754568,
					CheckInRadius = 50
				}
            };
            return Task.FromResult(locations);
        }

        public Task<IMiniJournal> FetchJournalAsync(IJournalIdentifiable id)
        {
            throw new NotImplementedException();
        }

		public async Task<string> TACheckInAsync(IIssueCheckInRequest<IJournalIdentifiable> request)
        {
			return await Task.Run(() => 
			{ 
                if (request.JourneyIdentifiable != null)
                {
                    ITAStatus status = TAStatusList.Where(a => a.JournalEntry.JournalId == request.JourneyIdentifiable.JournalId).FirstOrDefault();
                    status.CheckedInDateTime = DateTime.Now;
                }
                else
                {
    				TAStatusList.Add(new RotaGeek.RESTWeb.Api.Client.Models.TAStatus() 
    				{
                        TaId = (int)DateTime.Now.Ticks,
    					CheckedInDateTime = DateTime.Now, 
    					JournalEntry = (MiniJournal)JournalTestData.NightShiftTwoDays(DateTime.Today)
    				});
                }
				return "User checked in";
			});
        }

		public async Task<string> TACheckOutAsync(IIssueCheckOutRequest request)
        {
			return await Task.Run(() => 
			{ 

				ITAStatus status = TAStatusList.Where(a => a.TaId == request.TimeAndAttendanceId).FirstOrDefault();
				if(status!= null)
				{
					status.CheckedOutDateTime = DateTime.Now;
				}
				return "User checked out";
			});
        }

        public Task<IGroupProfileSettings> GroupProfileSettingsAsync()
        {
            return Task.FromResult((IGroupProfileSettings) new PublicModel.Poco.GroupProfileSettings()
            {
				LeaveEnabled = true,
				SwapsEnabled = true,
				WeekViewEnabled = true,
                TimeAndAttendanceEnabled = true,
				GeofencingEnabled = true,
                //AvailableShiftsEnabled = true,
            });
        }

        public Task<List<IMiniLocation>> LocationsForGroupAsync()
        {
            return LocationsForUserAsync();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

		public async Task<List<ITAStatus>> TAForUserAsync(DateTime datefrom, DateTime dateto)
		{
			return await Task.Run(() => 
			{ 
				if(TAStatusList == null)
				{
					TAStatusList = new List<ITAStatus>(); 
					int index=0;


					TAStatusList.Add(new RotaGeek.RESTWeb.Api.Client.Models.TAStatus() 
					{
						TaId = (int)DateTime.Now.Ticks,
						CheckedInDateTime = DateTime.Now.AddHours(2).AddMinutes(20), 
					});
					index++;

					TAStatusList.Add(new RotaGeek.RESTWeb.Api.Client.Models.TAStatus() 
					{
						TaId = (int)DateTime.Now.Ticks,
						CheckedInDateTime = DateTime.Now, 
					});
					index++;


					TAStatusList.Add(new RotaGeek.RESTWeb.Api.Client.Models.TAStatus() 
					{
                        TaId = (int)DateTime.Now.Ticks,
						CheckedInDateTime = DateTime.Now, 
						JournalEntry = (MiniJournal)JournalTestData.DayShiftTomorrow(DateTime.Today),
					});
					TAStatusList[index++].JournalEntry.JournalId = (int)DateTime.Now.Ticks;

					TAStatusList.Add(new RotaGeek.RESTWeb.Api.Client.Models.TAStatus() 
					{
                        TaId = (int)DateTime.Now.Ticks,
						CheckedInDateTime = DateTime.Now, 
						CheckedOutDateTime= DateTime.Now.AddHours(4),
						JournalEntry = (MiniJournal)JournalTestData.NightShiftTwoDays(DateTime.Today)
					});
					TAStatusList[index++].JournalEntry.JournalId = (int)DateTime.Now.Ticks;


					TAStatusList.Add(new RotaGeek.RESTWeb.Api.Client.Models.TAStatus() 
					{
                        TaId = (int)DateTime.Now.Ticks,
						JournalEntry = (MiniJournal)JournalTestData.DayShiftToday(DateTime.Today)
					});
					TAStatusList[index++].JournalEntry.JournalId = (int)DateTime.Now.Ticks;


				}
				return TAStatusList;
			});
		}

        public Task UpdatePushNotificationToken(string token)
        {
            return Task.Run(() => { });
        }

		public Task UpdatePushNotificationToken(string token, string deviceId)
		{
			throw new NotImplementedException();
		}

        private IEnumerable<PublicModel.Poco.AvailableShift> _shifts;

        public async Task<PublicModel.Poco.AvailableShiftsModel> GetAvailableShiftsAsync(int page, int pageSize)
        {
            var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 9, 30, 0);
            var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 18, 30, 0);

            _shifts = Enumerable.Range(0, pageSize).Select(i => new PublicModel.Poco.AvailableShift
            {
                Id = (i + 1) * (page + 1),
                Location = "London",
                Start = start.AddDays(i),
                End = end.AddDays(i),
                Status = i % 3,
                Break = 30,
                Note = i % 2 == 0 ? null : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget dui nec magna dictum vehicula pharetra sit amet sem. Ut finibus commodo mi, vel pharetra eros volutpat nec. Praesent magna mi, semper eget lacus sed, rutrum pellentesque libero. Donec commodo at dolor non porttitor. Donec tincidunt condimentum libero sit amet fermentum."
            });

            return new PublicModel.Poco.AvailableShiftsModel
            {
                AvailableShifts = _shifts,
                Pagination = new PublicModel.Poco.Page
                {
                    PageNumber = page,
                    PageSizeRequested = pageSize,
                    TotalPages = page + 1,
                    TotalResults = (page + 1) * pageSize
                }
            };
        }

        public async Task<PublicModel.Poco.AvailableShift> RequestAvailableShiftAsync(PublicModel.Poco.AvailableShift shift)
        {
            var availableShift = _shifts.Single(s => s.Id == shift.Id && s.Type == shift.Type);
            availableShift.Status = 1;
            return availableShift;
        }

        public async Task<PublicModel.Poco.AvailableShift> UnrequestAvailableShiftAsync(PublicModel.Poco.AvailableShift shift)
        {
            var availableShift = _shifts.Single(s => s.Id == shift.Id && s.Type == shift.Type);
            availableShift.Status = 0;
            return availableShift;
        }

        public async Task RecordFailedCheckInOutAsync(PublicModel.Poco.FailedCheckin failure)
        {
            await Task.Run(() => { });
        }
    }
}
