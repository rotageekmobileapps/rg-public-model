namespace RotaGeek.PublicModel
{
    /// <summary>
    /// This class is deliberately meant to be used as a reference.
    /// RgClient will update the Value as required
    /// </summary>
    public class RefreshToken
    {
        public string UserId { get; private set; }
        public string Value { get; set; }

        public RefreshToken(string userId, string value)
        {
            UserId = userId;
            Value = value;
        }

        public void UpdateFrom(RefreshToken other)
        {
            if(ReferenceEquals(this, other)) return;

            this.UserId = other.UserId;
            this.Value = other.Value;
        }
    }
}
