namespace RotaGeek.PublicModel.Enums
{
    public enum LeaveStatus
    {
        Outstanding = 29,
        Approved = 30,
        NotApproved=31,
        InProcess=32
    }
}