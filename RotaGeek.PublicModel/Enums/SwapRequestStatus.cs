namespace RotaGeek.PublicModel.Enums
{
    public enum SwapRequestStatus
    {
        Submitted = 33,
        Accepted = 34,
        Rejected = 35,
        OnHold = 36,
        InProcess = 37
    }
}
