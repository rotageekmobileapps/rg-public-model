﻿using System;

namespace RotaGeek.PublicModel.Interface
{
    public interface IMiniLeaveType
    {
        int LeaveTypeId { get; set;  }
        string Description { get; set; }

        /// <summary>
        ///     This is the time when the corresponding journal entry would start, relative to the day on-which the leave is
        /// </summary>
        TimeSpan DiaryStart { get; set; }

        /// <summary>
        ///     This is the time when the corresponding journal entry would start, relative to the day on-which the leave is
        /// </summary>
        TimeSpan DiaryEnd { get; set; }

        int PayrollMinutes { get; set; }
        string Notes { get; set; }

        string Color { get; set; }

        string Icon { get; set; }
    }
}