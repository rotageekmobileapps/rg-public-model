﻿using System;

namespace RotaGeek.PublicModel.Interface
{
    public interface IIssueSwapRequestModel<TJournalIdentifiable>
        where TJournalIdentifiable : IJournalIdentifiable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        TJournalIdentifiable Journal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string AdditionalInformation { get; set; }
        #endregion
    }
}
