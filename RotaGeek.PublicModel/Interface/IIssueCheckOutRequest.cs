using System;
using RotaGeek.PublicModel.Poco;

namespace RotaGeek.PublicModel.Interface
{
    public interface IIssueCheckOutRequest
    {
        int TimeAndAttendanceId { get; set; }
        IPosition GeoPosition { get; set; }
    }
}