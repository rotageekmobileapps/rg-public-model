﻿
namespace RotaGeek.PublicModel.Interface
{
    public interface IRgUserSettings : IServerResource
    {
        string Email { get; set; }
        string Mobile { get; set; }
        
        bool EmailAlertsEnabled { get; set; }
        bool MobileAlertsEnabled { get; set; }
    }
}