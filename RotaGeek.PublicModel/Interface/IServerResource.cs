﻿using System;

namespace RotaGeek.PublicModel.Interface
{
    public static class IdentityExtensions
    {
        public static Guid ToGuid(this int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }
    }
    

    public interface IServerResource
    {
        /// <summary>
        /// This should return the value of the db assigned identity key for this item as a GUID.
        /// If the item id is an int then use the ToGuid() extension to generate a consistent GUID.
        /// If the item is new then it should return either null or the "Zero Guid": 0.ToGuid().
        /// 
        /// We use this to determine if an item is expected to exist in the db and also potentially for cacheing and other such things.
        /// </summary>
        Guid? IdentifierAsGuid();

        /// <summary>
        /// Used to enforce optimistic concurrency
        /// </summary>
        Guid ETag { get; set; }

        //TODO: FetchedAt strictly only required on the client - refactor accordingly
        //Implementing as a method because we don't need to serialize this - it's determined entirely on the client side
        DateTime FetchedAt();
    }
}