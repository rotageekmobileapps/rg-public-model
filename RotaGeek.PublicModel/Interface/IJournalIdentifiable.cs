namespace RotaGeek.PublicModel.Interface
{
    public interface IJournalIdentifiable : IServerResource
    {
        int JournalId { get; set; }
    }
}