﻿namespace RotaGeek.PublicModel.Interface
{
    public interface IMiniLocation
    {
        string Name { get; set; }
        //TODO: strictly we should make an ILocationIdentifiable with an ETag - but they change pretty rarely so I think its safe enough
        string LocationId { get; set; }

        double? Longitude { get; set; }
        double? Latitude { get; set; }
        double? CheckInRadius { get; set; }
    }
}