using System;

namespace RotaGeek.PublicModel.Interface
{
    public interface IRgClient : IDisposable
    {
        IRgDataClient DataClient { get; }
    }
}