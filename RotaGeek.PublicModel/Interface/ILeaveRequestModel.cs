﻿using System;

namespace RotaGeek.PublicModel.Interface
{
    public interface IIssueLeaveRequestModel 
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        DateTime LeaveStartOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        DateTime LeaveEndOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Reason { get; set; }
        /// <summary>
        /// 
        /// </summary>
        int LeaveTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Notes { get; set; }
        #endregion
    }
}
