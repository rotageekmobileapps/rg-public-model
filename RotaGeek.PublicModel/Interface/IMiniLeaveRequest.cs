using System;
using RotaGeek.PublicModel.Enums;

namespace RotaGeek.PublicModel.Interface
{
    public interface IMiniLeaveRequest : IMiniLeaveRequest<LeaveStatus> { }
    public interface IMiniLeaveRequest<out TLeaveStatus> : IServerResource
    {
        int LeaveRequestId { get; set; }
        //int? UserId { get; set; }
        DateTime LeaveStartOn { get; set; }
        DateTime LeaveEndOn { get; set; }
        string Reason { get; set; }

        string Description { get; set; }
        string Icon { get; set; }
        string Color { get; set; }

        //ToDo is this nullable?
        int? LeaveTypeId { get; set; }

        //TODO: dont send unlimited length strings!
        string Notes { get; set; }
        //TODO: dont send unlimited length strings!
        string Response { get; set; }

        [Obsolete("Use Status() instead", false)]
        int StatusId { get; set; }

        TLeaveStatus Status();

        //TODO: Is this necessary?
        DateTime? CreatedOn { get; set; }
    }
}