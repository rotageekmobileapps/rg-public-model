﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotaGeek.PublicModel.Interface
{
    public interface IPosition
    {
        double Latitude { get; }
        double Longitude { get; }
        DateTime Timestamp { get; }
    }
}
