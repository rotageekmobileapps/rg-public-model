﻿namespace RotaGeek.PublicModel.Interface
{
    public interface IRgUserConfiguration
    {
        bool TimeAndAttendanceEnabled { get; }
        bool SwapRequestsEnabled { get; }
    }
}