using System;
using RotaGeek.PublicModel.Enums;

namespace RotaGeek.PublicModel.Interface
{
    public interface IMiniSwapRequest : IMiniSwapRequest<SwapRequestStatus>
    {
        
    }
    public interface IMiniSwapRequest<out TStatusEnum>
    {
        int SwapRequestId { get; set; }
        int? RequesterUserId { get; set; }
        int? SwappedWithUserId { get; set; }
        int? JournalId { get; set; }
        DateTime? Seen { get; set; }
        string Notes { get; set; }
        
        DateTime? CreatedOn { get; set; }
        string Response { get; set; }
        string StartingDOW { get; set; }
        IMiniJournal JournalEntry { get; }
        /// <summary>
        /// Use Status() method instead to get the enum.
        /// This is only here to allow sending the int over the wire
        /// </summary>
        [Obsolete("Use Status() instead to get the enum")]
        int StatusId { get; set; }

        TStatusEnum Status();
    }


    public interface IMiniSwapRequest<out TMiniJournal, out TStatusEnum> : IMiniSwapRequest<TStatusEnum>, IServerResource
        where TMiniJournal : IMiniJournal
    {
        new TMiniJournal JournalEntry { get; }

    }
}