namespace RotaGeek.PublicModel.Interface
{
    public interface IUserProfileSettings : IServerResource
    {
        string Email { get; set; }
        string Mobile { get; set; }
        bool? ReceiveBySMS { get; set; }
        bool? ReceiveByEmail { get; set; }
    }
}