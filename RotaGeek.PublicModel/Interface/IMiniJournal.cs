using System;
using System.Collections.Generic;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel
{
    /// <summary>
    ///     MiniJournal might be work, might be leave, might be something else
    ///     We don't specify - but we provide a color and an Icon and a description
    /// </summary>
    public interface IMiniJournal : IJournalIdentifiable
    {
        string Description { get; set; }
        DateTime StartDateTime { get; set; }
        DateTime EndDateTime { get; set; }

        /// <summary>
        ///     This is hex code (without the # prefix)
        /// </summary>
        /// <summary>
        ///     Name of the person with the shift/leave
        /// </summary>
        string name { get; set; } // TODO: 'N'ame

        /// <summary>
        ///     Text note with further information about the shift/leave
        /// </summary>
        List<string> Notes { get; set; }

        string Colour { get; set; }
        //TODO: make Icon an enum?
        /// <summary>
        ///     This is only for leave types and can have values
        ///     "plane", "bolt", "truck", "time", "glass", "trophy", "picture", "dollar", "bell", "archive", "sun", "moon",
        ///     "hospital" or "medkit"
        /// </summary>
        string Icon { get; set; }

        /// <summary>
        /// This is the location name! Not the location id!
        /// </summary>
        string Location { get; set; }
        bool HasCheckedIn { get; set; }
        bool HasCheckedOut { get; set; }
        bool HasSwap { get; set; }
        bool CanRequestDayOff { get; }
        bool CanRequestSwap { get; }

		bool isLeave { get; }

    }
}