using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using RotaGeek.PublicModel.Enums;
using RotaGeek.PublicModel.Poco;

namespace RotaGeek.PublicModel.Interface
{
    public interface IRgDataClient : IRgDataClient<
            IMiniJournal,
            IMiniSwapRequest<SwapRequestStatus>,
            IMiniLeaveRequest,
            IIssueSwapRequestModel<IJournalIdentifiable>,
            IIssueLeaveRequestModel,
            IJournalIdentifiable,
            ITAStatus,
            IUserProfileSettings,
            IMiniLeaveType,
            IMiniLocation,
            IIssueCheckInRequest<IJournalIdentifiable>,
            IIssueCheckOutRequest,
            IGroupProfileSettings
            >
    { }


    public interface IRgDataClient<
        TMiniJournal,
        TMiniSwapRequest,
        TMiniLeaveRequest,
        in TIssueSwapRequestModel,
        in TIssueLeaveRequestModel,
        TJournalIdentifiable,
        TTAStatus,
        TUserProfileSettings,
        TMiniLeaveType,
        TMiniLocation,
        TIssueCheckInRequest,
        TIssueCheckOutRequest,
        TGroupProfileSettings
        >
        : IDisposable 
        where TMiniJournal : IMiniJournal
        where TMiniSwapRequest : IMiniSwapRequest<SwapRequestStatus>
        where TMiniLeaveRequest : IMiniLeaveRequest
        where TIssueSwapRequestModel : IIssueSwapRequestModel<TJournalIdentifiable>
        where TIssueLeaveRequestModel : IIssueLeaveRequestModel 
        where TJournalIdentifiable : IJournalIdentifiable
        where TTAStatus : ITAStatus
        where TUserProfileSettings : IUserProfileSettings
        where TMiniLeaveType : IMiniLeaveType
        where TMiniLocation : IMiniLocation
        where TIssueCheckInRequest : IIssueCheckInRequest<TJournalIdentifiable>
        where TIssueCheckOutRequest : IIssueCheckOutRequest
        where TGroupProfileSettings : IGroupProfileSettings
    {
        Task<List<TMiniJournal>> JournalEntriesForUserAsync(DateTime datefrom, DateTime dateto);

        Task<List<TMiniJournal>> JournalEntriesForWeekViewAsync(DateTime datefrom, DateTime dateto);

        //TODO: This should probably either page or have a date range and/or some way to set if the client wants "active" or archived swap requests.
        //We want to not send all sqap requests ever!
        Task<List<TMiniSwapRequest>> SwapRequestsForUserAsync();

        //TODO: This should probably either page or have a date range and/or some way to set if the client wants "active" or archived leave requests.
        Task<List<TMiniLeaveRequest>> LeaveRequestsForUserAsync();
        Task<List<TTAStatus>> TAForUserAsync(DateTime datefrom, DateTime dateto);

        //TODO: Consider returning the swap request
        Task<HttpResponseMessage> IssueSwapRequestAsyncMsg(TIssueSwapRequestModel issueSwapRequestModel);
        
        //TODO: Make Leave Type Id strong type ... with ETag?
        Task<string> IssueLeaveRequestAsync(TIssueLeaveRequestModel issueLeaveRequestModel);

        Task<TUserProfileSettings> UserProfileSettingsAsync();

        Task<TUserProfileSettings> UserProfileSettingsAsync(TUserProfileSettings userProfileSettings);

        Task<List<TMiniLeaveType>> LeaveTypesForLocationAsync(string locationId);

        Task<List<TMiniLocation>> LocationsForUserAsync();

        Task<TMiniJournal> FetchJournalAsync(TJournalIdentifiable id);

        Task<string> TACheckInAsync(TIssueCheckInRequest request);

        Task<string> TACheckOutAsync(TIssueCheckOutRequest request);

        Task RecordFailedCheckInOutAsync(FailedCheckin failure);

        Task<TGroupProfileSettings> GroupProfileSettingsAsync();

        Task<List<TMiniLocation>> LocationsForGroupAsync();

        Task UpdatePushNotificationToken(string token, string deviceId);
        Task<AvailableShiftsModel> GetAvailableShiftsAsync(int page, int pageSize);
        Task<AvailableShift> RequestAvailableShiftAsync(AvailableShift shift);
        Task<AvailableShift> UnrequestAvailableShiftAsync(AvailableShift shift);
    }
}

