namespace RotaGeek.PublicModel.Interface
{
    public interface IGroupProfileSettings
    {
        bool TimeAndAttendanceEnabled { get; set; }

        bool GeofencingEnabled { get; set; }

        bool WeekViewEnabled { get; set; }

        bool SwapsEnabled { get; set; }

        bool LeaveEnabled { get; set; }

        int StartDayOfWeek { get; set; }

        bool AvailableShiftsEnabled { get; set; }
    }
}