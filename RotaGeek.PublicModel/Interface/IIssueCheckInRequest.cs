using System;
using RotaGeek.PublicModel.Poco;

namespace RotaGeek.PublicModel.Interface
{
    public interface IIssueCheckInRequest : IIssueCheckInRequest<IJournalIdentifiable> { }
    public interface IIssueCheckInRequest<TJournalIdentifiable>
        where TJournalIdentifiable : IJournalIdentifiable
    {
        TJournalIdentifiable JourneyIdentifiable { get; set; }
        string Notes { get; set; }
        IPosition GeoPosition { get; set; }

        string LocationId { get; set; }
    }
}