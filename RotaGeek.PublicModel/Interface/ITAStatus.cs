using System;
using RotaGeek.PublicModel.Poco;

namespace RotaGeek.PublicModel.Interface
{
    public interface ITAStatus
    {
        int TaId { get; set; }
        DateTime? CheckedInDateTime { get; set; }
        DateTime? CheckedOutDateTime { get; set; }

        IMiniJournal JournalEntry { get; }
    }
    public interface ITAStatus<TMiniJournal> : ITAStatus
    {
        new TMiniJournal JournalEntry { get; set; }
    }
}