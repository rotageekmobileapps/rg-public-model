﻿namespace RotaGeek.PublicModel
{
    public class AppDefinition
    {
        public string Device { get; set; }
        public string OperatingSystem { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }

        public AppDefinition(string device, string operatingSystem, string appName, string appVersion)
        {
            Device = device;
            OperatingSystem = operatingSystem;
            AppName = appName;
            AppVersion = appVersion;
        }
    }
}
