namespace RotaGeek.PublicModel
{
    /// <summary>
    /// Contains a RefreshToken along with the url for the server on which it is valid
    /// </summary>
    public class FlavouredRefreshToken
    {
        public RefreshToken RefreshToken { get; private set; }
        public string ServerUrl { get; private set; }

        public FlavouredRefreshToken(RefreshToken refreshToken, string serverUrl)
        {
            RefreshToken = refreshToken;
            ServerUrl = serverUrl;
        }
    }
}
