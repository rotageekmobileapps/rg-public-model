﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RotaGeek.PublicModel.Interface;
using RotaGeek.PublicModel.Poco;

namespace RotaGeek.PublicModel.Extensions
{
    public static class TAStatusExtensions
    {
        public static bool CheckedIn(this ITAStatus status)
        {
            return status.CheckedInDateTime.HasValue;
        }
        public static bool CheckedOut(this ITAStatus status)
        {
            return status.CheckedOutDateTime.HasValue;
        }
    }
}
