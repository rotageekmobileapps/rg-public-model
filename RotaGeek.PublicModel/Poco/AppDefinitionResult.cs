﻿namespace RotaGeek.PublicModel.Poco
{
    /// <summary>
    /// Contains details of the failure of an AppDefinition check
    /// </summary>
    public class AppDefinitionResult
    {
        public bool IsAppNameIncompatible { get; }
        public bool IsAppVersionIncompatible { get; }
        public bool IsOperatingSystemIncompatible { get; }
        public bool IsDeviceIncompatible { get; }

        /// <summary>
        /// True if there is any incompatibility
        /// </summary>
        public bool IsAppDefinitionInvalid => IsAppNameIncompatible || IsAppVersionIncompatible || IsDeviceIncompatible || IsOperatingSystemIncompatible;

        /// <summary>
        /// Creates a new AppDefinitionResult with the given details of incompatibilities
        /// </summary>
        /// <param name="isAppNameIncompatible">The name of the app is incompatible</param>
        /// <param name="isAppVersionIncompatible">The version of the app is incompatible</param>
        /// <param name="isOsIncompatible">The operating system is incompatible</param>
        /// <param name="isDeviceIncompatible">The device is incompatible</param>
        public AppDefinitionResult(bool isAppNameIncompatible, bool isAppVersionIncompatible, bool isOsIncompatible, bool isDeviceIncompatible)
        {
            IsAppNameIncompatible = isAppNameIncompatible;
            IsAppVersionIncompatible = isAppVersionIncompatible;
            IsOperatingSystemIncompatible = isOsIncompatible;
            IsDeviceIncompatible = isDeviceIncompatible;
        }

        /// <summary>
        /// Creates an AppDefinitionResult from the byte data obtained using appDefinitionResult.ToByte()
        /// </summary>
        /// <param name="data">The data with which to create the AppDefinitionResult</param>
        /// <returns></returns>
        public static AppDefinitionResult FromByte(byte data)
        {
            return new AppDefinitionResult((data & (1)) != 0, (data & (1 << 1)) != 0, (data & (1 << 2)) != 0, (data & (1 << 3)) != 0);
        }

        /// <summary>
        /// Serialises the class into a byte which can be used with FromByte to retrieve the data
        /// </summary>
        /// <returns>A byte which represents the values in the class to be used with FromByte</returns>
        public byte ToByte()
        {
            byte result = 0; // 0000[Device?1:0][OS?1:0][Version?1:0][Name?1:0]

            if (IsAppNameIncompatible) result |= 1;
            if (IsAppVersionIncompatible) result |= (1 << 1);
            if (IsOperatingSystemIncompatible) result |= (1 << 2);
            if (IsDeviceIncompatible) result |= (1 << 3);

            return result;
        }
    }
}