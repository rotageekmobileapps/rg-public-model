using System;
using RotaGeek.PublicModel.Enums;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    /// <summary>
    /// This is used by the existing Web Service so be careful with it
    /// </summary>
    public class MiniSwapRequest<TMiniJournal> : ServerResource, 
        IMiniSwapRequest<TMiniJournal, SwapRequestStatus>
        where TMiniJournal : IMiniJournal
    {
        public int SwapRequestId { get; set; }
        public int? RequesterUserId { get; set; }
        public int? SwappedWithUserId { get; set; }
        public int? JournalId { get; set; }
        public DateTime? Seen { get; set; }
        public TMiniJournal JournalEntry { get; set; }
        public SwapRequestStatus Status()
        {
            return (SwapRequestStatus) StatusId;
        }

        public int StatusId { get; set; }
        public string Notes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Response { get; set; }
        public string StartingDOW { get; set; }

        IMiniJournal IMiniSwapRequest<SwapRequestStatus>.JournalEntry
        {
            get { return this.JournalEntry; }
        }

        public override Guid? IdentifierAsGuid() => SwapRequestId.ToGuid();
    }
}