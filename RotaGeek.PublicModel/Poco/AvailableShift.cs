﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotaGeek.PublicModel.Poco
{
    public class AvailableShift
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Break { get; set; }
        public string Note { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
    }

    public class AvailableShiftsModel
    {
        public IEnumerable<AvailableShift> AvailableShifts { get; set; }
        public Page Pagination { get; set; }
    }
}
