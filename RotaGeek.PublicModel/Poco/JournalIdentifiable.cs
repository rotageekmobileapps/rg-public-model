using System;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class JournalIdentifiable : ServerResource, IJournalIdentifiable
    {
        public override Guid? IdentifierAsGuid()
        {
            return JournalId.ToGuid();
        }

        public int JournalId { get; set; }
    }
}