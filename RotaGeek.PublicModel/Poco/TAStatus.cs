using System;
using System.Collections.Generic;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class TAStatus : ITAStatus
    {
        public int TaId { get; set; }
        //public int UserId { get; set; }
        //public string Username { get; set; }
        public DateTime? CheckedInDateTime { get; set; }
        public DateTime? CheckedOutDateTime { get; set; }
        public MiniJournal JournalEntry { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }

        public List<string> Notes { get; set; }

        IMiniJournal ITAStatus.JournalEntry => this.JournalEntry;
    }
}