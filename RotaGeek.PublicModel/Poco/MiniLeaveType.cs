﻿using System;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class MiniLeaveType : IMiniLeaveType
    {
        public int LeaveTypeId { get; set; }
        public string Description { get; set; }

        /// <summary>
        ///     This is the time when the corresponding journal entry would start, relative to the day on-which the leave is
        /// </summary>
        public TimeSpan DiaryStart { get; set; }

        /// <summary>
        ///     This is the time when the corresponding journal entry would start, relative to the day on-which the leave is
        /// </summary>
        public TimeSpan DiaryEnd { get; set; }

        public int PayrollMinutes { get; set; }
        public string Notes { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }
    }
}