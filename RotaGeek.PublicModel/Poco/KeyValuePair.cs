namespace RotaGeek.PublicModel.Poco
{
    /// <summary>
    /// This is used by the existing Web Service so be careful with it
    /// </summary>
    public class KeyValuePair
    {
        public int? key { get; set; }
        public string value { get; set; }
    }
}