using System;
using System.Runtime.Serialization;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class GroupProfileSettings : ServerResource, IGroupProfileSettings
    {
        [IgnoreDataMember]
        public int UserId;

        public bool TimeAndAttendanceEnabled { get; set; }
        public bool GeofencingEnabled { get; set; }

        public override Guid? IdentifierAsGuid() => UserId.ToGuid();

        public int StartDayOfWeek { get; set; }

        public bool WeekViewEnabled { get; set; }

        public bool SwapsEnabled { get; set; }

        public bool LeaveEnabled { get; set; }

        public bool AvailableShiftsEnabled { get; set; }
    }
}