﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotaGeek.PublicModel.Poco
{
    public class Page
    {
        public int PageNumber { get; set; }
        public int PageSizeRequested { get; set; }
        public int TotalResults { get; set; }
        public int TotalPages { get; set; }
    }
}
