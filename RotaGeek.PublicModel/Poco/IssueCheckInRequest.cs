﻿using System;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class IssueCheckInRequest : IIssueCheckInRequest
    {
        public string LocationId { get; set; }
        public IJournalIdentifiable JourneyIdentifiable { get; set; }
        public string Notes { get; set; }
        public IPosition GeoPosition { get; set; }
    }
}
