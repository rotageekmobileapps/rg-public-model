﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotaGeek.PublicModel.Poco
{
    public class FailedCheckin
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public bool IsCheckIn { get; set; }
        public int LocationId { get; set; }
        public DateTime Date { get; set; }
    }
}
