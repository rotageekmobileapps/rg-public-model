﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class MiniLocation : IMiniLocation
    {
        public string Name { get; set; }
        public string LocationId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public double? CheckInRadius { get; set; }
    }
}
