using System;
using RotaGeek.PublicModel.Enums;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    /// <summary>
    /// This is used by the existing Web Service so be careful with it
    /// </summary>
    public class MiniLeaveRequest : ServerResource, IMiniLeaveRequest
    {
        public int LeaveRequestId { get; set; }
        public int? UserId { get; set; }
        public DateTime LeaveStartOn { get; set; }
        public DateTime LeaveEndOn { get; set; }
        public string Reason { get; set; }
        public int? LeaveTypeId { get; set; }
        public string Notes { get; set; }
        public string Response { get; set; }
        public int StatusId { get; set; }

        public LeaveStatus Status()
        {
            return (LeaveStatus) StatusId;
        }
        public DateTime? CreatedOn { get; set; }
        public string LeaveColour { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }

        public override Guid? IdentifierAsGuid() => LeaveRequestId.ToGuid();
    }
}