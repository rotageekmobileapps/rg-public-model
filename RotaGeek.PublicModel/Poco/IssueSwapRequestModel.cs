using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class IssueSwapRequestModel : IIssueSwapRequestModel<IJournalIdentifiable>
    {
        public IJournalIdentifiable Journal { get; set; }

        public string AdditionalInformation { get; set; }
    }
}