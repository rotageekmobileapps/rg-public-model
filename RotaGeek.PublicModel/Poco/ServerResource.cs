﻿using System;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public abstract class ServerResource : IServerResource
    {
        private readonly DateTime _constructedAt;

        protected ServerResource()
        {
            //Assume that the item is constructed pretty soon after data is fetched
            _constructedAt = DateTime.Now;
        }

        public abstract Guid? IdentifierAsGuid();
        public Guid ETag { get; set; }

        /// <summary>
        ///     This is an approximate measurement (in the time frame of the client) of when the data was fetched from the server
        ///     Use to indicate to the user if the data is stale etc.
        ///     ETag is used to actually deal with concurrency
        /// </summary>
        /// <returns></returns>
        public DateTime FetchedAt()
        {
            return _constructedAt;
        }
    }
}