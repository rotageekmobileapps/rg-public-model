using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    /// <summary>
    /// This is used by the existing Web Service so be careful with it
    /// </summary>
    public class MiniJournal : ServerResource, IMiniJournal
    {
        public int? UserId { get; set; }
        public string name { get; set; }

        [DataMember(EmitDefaultValue = false, IsRequired = false)]
        public List<string> Notes { get; set; }
        public int JournalId { get; set; }
        public string Description { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Colour { get; set; }
        public string Icon { get; set; }

        [DataMember(EmitDefaultValue = true, IsRequired = false)]
        public string ShiftNotes { get; set; }
        public string ShiftColour { get; set; }
        public bool isLeave { get; set; }
        public bool isShift { get; set; }
        public string Location { get; set; }
        public int? LocationId { get; set; }
        public bool HasCheckedIn { get; set; }
        public bool HasCheckedOut { get; set; }
        public bool HasSwap { get; set; }
        public bool CanRequestDayOff { get; set; }
        public bool CanRequestSwap { get; set; }
        public string HasShift { get; set; }
        public List<KeyValuePair> LeaveTypes { get; set; }
        public string StartingDOW { get; set; }
        public int? HasTARecord { get; set; }
        public override Guid? IdentifierAsGuid() => JournalId.ToGuid();
    }
}