using System;
using System.Runtime.Serialization;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class UserProfileSettings : ServerResource, IUserProfileSettings
    {
        [IgnoreDataMember]
        public int UserId;
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool? ReceiveBySMS { get; set; }
        public bool? ReceiveByEmail { get; set; }
        public override Guid? IdentifierAsGuid() => UserId.ToGuid();
        
    }
}