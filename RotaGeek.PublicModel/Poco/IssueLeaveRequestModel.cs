using System;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class IssueLeaveRequestModel : IIssueLeaveRequestModel
    {
        public IssueLeaveRequestModel() { }
        public IssueLeaveRequestModel(DateTime leaveStartOn, DateTime leaveEndOn, string reason, int leaveTypeId, string notes)
        {
            LeaveStartOn = leaveStartOn;
            LeaveEndOn = leaveEndOn;
            Reason = reason;
            LeaveTypeId = leaveTypeId;
            Notes = notes;
        }

        public DateTime LeaveStartOn { get; set; }

        public DateTime LeaveEndOn { get; set; }

        public string Reason { get; set; }

        public int LeaveTypeId { get; set; }

        public int LocationId { get; set; }

        public string Notes { get; set; }
    }
}