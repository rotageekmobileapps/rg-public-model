﻿using System;
using RotaGeek.PublicModel.Interface;

namespace RotaGeek.PublicModel.Poco
{
    public class IssueCheckOutRequest : IIssueCheckOutRequest
    {
        public int TimeAndAttendanceId { get; set; }
        public IPosition GeoPosition { get; set; }
    }
}
